import win32com.client
import serial
#pywin32


# wmi = win32com.client.GetObject ("winmgmts:")
# for usb in wmi.InstancesOf ("Win32_USBHub"):
#    print(usb.DeviceID)

# https://raspberrypi.stackexchange.com/questions/3465/usb-hid-device-only-firing-1-event
# http://pyusb.github.io/pyusb/



import sys
import glob
import serial
from datetime import datetime


def serial_ports():
    """ Lists serial port names

        :raises EnvironmentError:
            On unsupported or unknown platforms
        :returns:
            A list of the serial ports available on the system
    """
    if sys.platform.startswith('win'):
        ports = ['COM%s' % (i + 1) for i in range(256)]
    elif sys.platform.startswith('linux') or sys.platform.startswith('cygwin'):
        # this excludes your current terminal "/dev/tty"
        ports = glob.glob('/dev/tty[A-Za-z]*')
    elif sys.platform.startswith('darwin'):
        ports = glob.glob('/dev/tty.*')
    else:
        raise EnvironmentError('Unsupported platform')

    result = []
    for port in ports:
        try:
            s = serial.Serial(port)
            s.close()
            result.append(port)
        except (OSError, serial.SerialException):
            pass
    return result


if __name__ == '__main__':
    print(serial_ports())
    counter = 0
    s = serial.Serial("COM3")
    start_time = datetime.timestamp(datetime.now())
    received_data = 0
    print_counter = 0
    while s.is_open:
        # s.write("test")
        local_read = s.read_all()

        data_size = len(local_read)

        if(data_size > 0):
            counter = counter + 1
            received_data = float(received_data + data_size)

            print_counter = print_counter + 1
            if(print_counter >= 1000):
                local_time = datetime.timestamp(datetime.now())
                deltatime = local_time - start_time
                speed = received_data/ deltatime
                print("Packet N" + str(counter) + ", size:" + str(data_size) + ", speed: " + str(speed)) #  + " " + str(local_read))
                print_counter = 0
    s.close()


