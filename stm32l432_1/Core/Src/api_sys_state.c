/*
 * api_sys_state.c
 *
 *  Created on: Dec 18, 2019
 *      Author: volokitin
 */

#include "api_sys_state.h"
#include "api_timers.h"
#include "api_communication.h"
#include "api_adc.h"

eSysState e_system_state = SYSTEM_IDLE;


void main_function(void)
{

	switch(e_system_state)
	{
		case SYSTEM_IDLE:
			break;
		case START_READ_SEQUENSE:
			// call start read sequence function
			api_sys_set_state(WAITING_END_OF_READ_SEQUENCE);
			HAL_TIM_Base_Start_IT(&htim6);
			break;
		case WAITING_END_OF_READ_SEQUENCE:
			api_sys_set_state(SYSTEM_IDLE);
			break;
		case SEND_DATA_TO_UART:
			// function send data to uart
			api_adc_send_adc_data();
			api_sys_set_state(SYSTEM_IDLE);
			break;
		default:
			break;
	}

}

void api_sys_set_state(eSysState e_value)
{
	e_system_state = e_value;
}


void api_sys_get_state(eSysState * pe_value)
{
	*pe_value = e_system_state;
}






