/*
 * api_uart_freertos_dma.c
 *
 *  Created on: Nov 5, 2019
 *      Author: volokitin
 */

#include "api_uart_dma.h"

#ifdef GPIO_ON
	#include "api_gpio.h"
#endif

#define UART_USART1_DMA_RX_BUFFER_SIZE 1024
#define UART_USART2_DMA_RX_BUFFER_SIZE 1024

uint8_t buffer_USART1_RX[UART_USART1_DMA_RX_BUFFER_SIZE];
uint8_t buffer_USART2_RX[2];//UART_USART2_DMA_RX_BUFFER_SIZE];

static uint8_t u8_overflow_USART2_flag = 0;
static uint8_t u8_overflow_USART1_flag = 0;


static uint8_t u8_end_tx_USART1_flag = 1;
static uint8_t u8_end_tx_USART2_flag  = 1;

static size_t dma_USART2_tail = 0;
static size_t dma_USART1_tail = 0;

HAL_StatusTypeDef UART_ID_init(uint8_t u8_interface_id)
{
	HAL_StatusTypeDef rdr_return = HAL_OK;

	switch(u8_interface_id)
	{
		case 0:
			u8_end_tx_USART1_flag = 1;
			u8_overflow_USART1_flag = 0;
			HAL_UART_Receive_DMA(&huart1, buffer_USART1_RX, UART_USART1_DMA_RX_BUFFER_SIZE);
			break;
		case 1:
			u8_end_tx_USART2_flag  = 1;
			u8_overflow_USART2_flag = 0;
			HAL_UART_Receive_DMA(&huart2, buffer_USART2_RX, UART_USART2_DMA_RX_BUFFER_SIZE);
			break;
		default:
			rdr_return = HAL_ERROR;
	}

	return 	rdr_return;
}


void UART_flush(uint8_t u8_interface_id)
{
	switch(u8_interface_id)
	{
		case 0:
			u8_overflow_USART1_flag = 0;
			/* set tail to header position */
			dma_USART1_tail = UART_USART1_DMA_RX_BUFFER_SIZE - huart1.hdmarx->Instance->CNDTR;

		case 1:
			u8_overflow_USART2_flag = 0;
			/* set tail to header position */
			dma_USART2_tail = UART_USART2_DMA_RX_BUFFER_SIZE - huart2.hdmarx->Instance->CNDTR;
			break;
		default:
			break;
	}
}


void UART_USART1_read_c(uint8_t *pu8_value, HAL_StatusTypeDef *prdr_return)
{
	HAL_StatusTypeDef rdr_return = HAL_OK;
    size_t dma_head = 0;

    __disable_irq();
    dma_head = UART_USART1_DMA_RX_BUFFER_SIZE - huart1.hdmarx->Instance->CNDTR;
    __enable_irq();

    /* check if buffer empty: */
    if(dma_USART1_tail == dma_head)
    {
    	if(u8_overflow_USART1_flag == 0)
    	{
    		rdr_return = HAL_TIMEOUT;
    	}
    }

    /* If buffer isn't empty: */
    if(rdr_return == HAL_OK)
    {

		if((dma_USART1_tail < dma_head) && (u8_overflow_USART1_flag == 0))
		{
			*pu8_value = buffer_USART1_RX[dma_USART1_tail];
			dma_USART1_tail++;
			if(dma_USART1_tail >=UART_USART1_DMA_RX_BUFFER_SIZE)
			{
				dma_USART1_tail = 0;
				u8_overflow_USART1_flag = 0;
			}
		}

		/* Is round buffer true: */
		if(u8_overflow_USART1_flag != 0)
		{
			/* Old data was rewritten:
			 * "He had to have builded this bridge, but nothing was done."
			 * So, set tail to head: */
			if(dma_USART1_tail < dma_head)
			{
				dma_USART1_tail = dma_head;
			}
			/* In this case we can read us usual: */
			if(dma_USART1_tail >= dma_head)
			{
				*pu8_value = buffer_USART1_RX[dma_USART1_tail];
				dma_USART1_tail++;
				if(dma_USART1_tail >=UART_USART1_DMA_RX_BUFFER_SIZE)
				{
					dma_USART1_tail = 0;
					u8_overflow_USART1_flag = 0;
				}
			}
		}
    }

    *prdr_return = rdr_return;
}


/* Function used for manipulating with pail counter. In is used for reading and parsing packets: */
void get_dma_USART_tail(USART_TypeDef *usart, size_t *pst_tail)
{
	switch((uint32_t)usart)
	{
		case (uint32_t)USART1:
				*pst_tail = dma_USART1_tail;
			break;
		case (uint32_t)USART2:
				*pst_tail = dma_USART2_tail;
			break;
		default:
			break;
	}
}

void set_dma_USART_tail(USART_TypeDef *usart, size_t st_tail)
{
	switch((uint32_t)usart)
	{
		case (uint32_t)USART1:
				dma_USART1_tail = st_tail;
			break;
		case (uint32_t)USART2:
				dma_USART2_tail = st_tail;
			break;
		default:
			break;
	}

}



void UART_USART2_read_c(uint8_t *pu8_value, HAL_StatusTypeDef *prdr_return)
{
	HAL_StatusTypeDef rdr_return = HAL_OK;
    size_t dma_head = 0;

    __disable_irq();
    dma_head = UART_USART2_DMA_RX_BUFFER_SIZE - huart2.hdmarx->Instance->CNDTR;
    __enable_irq();

    /* check if buffer empty: */
    if(dma_USART2_tail == dma_head)
    {
    	if(u8_overflow_USART2_flag == 0)
    	{
    		rdr_return = HAL_TIMEOUT;
    	}
    }

    /* If buffer isn't empty: */
    if(rdr_return == HAL_OK)
    {

		if((dma_USART2_tail < dma_head) && (u8_overflow_USART2_flag == 0))
		{
			*pu8_value = buffer_USART2_RX[dma_USART2_tail];
			dma_USART2_tail++;
			if(dma_USART2_tail >=UART_USART2_DMA_RX_BUFFER_SIZE)
			{
				dma_USART2_tail = 0;
				u8_overflow_USART2_flag = 0;
			}
		}

		/* Is round buffer true: */
		if(u8_overflow_USART2_flag != 0)
		{
			/* Old data was rewritten:
			 * "He had to have builded this bridge, but nothing was done."
			 * So, set tail to head: */
			if(dma_USART2_tail < dma_head)
			{
				dma_USART2_tail = dma_head;
			}
			/* In this case we can read us usual: */
			if(dma_USART2_tail >= dma_head)
			{
				*pu8_value = buffer_USART2_RX[dma_USART2_tail];
				dma_USART2_tail++;
				if(dma_USART2_tail >=UART_USART2_DMA_RX_BUFFER_SIZE)
				{
					dma_USART2_tail = 0;
					u8_overflow_USART2_flag = 0;
				}
			}
		}
    }

    *prdr_return = rdr_return;
}


/* Read byte from RX buffer.
 * UART_HandleTypeDef *huart    	communication interface
 * uint8_t *pu8_value           	received value
 * HAL_StatusTypeDef *prdr_return   error code
 * */
void api_uart_read(USART_TypeDef *usart, uint8_t *pu8_value, HAL_StatusTypeDef *prdr_return)
{
	switch((uint32_t)usart)
	{
		case (uint32_t)USART1:
				UART_USART1_read_c(pu8_value, prdr_return);
			break;
		case (uint32_t)USART2:
				UART_USART2_read_c(pu8_value, prdr_return);
			break;
		default:
			break;
	}
}




void HAL_UART_RxCpltCallback(UART_HandleTypeDef *UartHandle)
{
	switch((uint32_t)UartHandle->Instance)
	{
		case (uint32_t)USART1:
			u8_overflow_USART1_flag = 1;
			break;
		case (uint32_t)USART2:
			//This callback will be called if DMA buffer full
			u8_overflow_USART2_flag = 1;
			break;
		default:
			break;
	}
}


HAL_StatusTypeDef UART_tx_c(UART_HandleTypeDef *UartHandle, uint8_t * pu8_buff, uint32_t u32_len)
{

	HAL_StatusTypeDef  dev_return = HAL_ERROR;
	uint8_t tx_end_flag = 0;
	uint32_t u32_timer_counter = 0;


	while(1)
	{
		switch((uint32_t)UartHandle->Instance)
		{
			case (uint32_t)USART1:
				tx_end_flag = u8_end_tx_USART1_flag;
				break;
			case (uint32_t)USART2:
				tx_end_flag = u8_end_tx_USART2_flag;
				break;
			default:
				break;
		}

		if(tx_end_flag == 1)
		{
			break;
		}
		else
		{
			u32_timer_counter++;
			if(u32_timer_counter > 10000)
			{
				dev_return = HAL_TIMEOUT;
				break;
			}
		}
	}



	switch((uint32_t)UartHandle->Instance)
	{
		case (uint32_t)USART1:
			u8_end_tx_USART1_flag = 0;
			break;
		case (uint32_t)USART2:
			u8_end_tx_USART2_flag = 0;
			break;
		default:
			break;
	}


	dev_return = HAL_UART_Transmit_DMA(UartHandle, pu8_buff, u32_len);


	return(dev_return);
}



void HAL_UART_TxCpltCallback(UART_HandleTypeDef *huart)
{
	switch((uint32_t)huart->Instance)
	{
		case (uint32_t)USART1:
			u8_end_tx_USART1_flag = 1;
			break;
		case (uint32_t)USART2:
			u8_end_tx_USART2_flag = 1;
			break;
		default:
			break;
	}
}

#if 0
void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
	switch((uint32_t)huart->Instance)
	{
		case (uint32_t)USART1:
			//u8_end_rx_USART1_flag = 1;
			break;
		case (uint32_t)USART2:
			//u8_end_rx_USART2_flag = 1;
			break;
		default:
			break;
	}
}

#endif



void api_usleep(uint32_t usec)
{
	volatile uint32_t u32_i = 0;
	volatile uint32_t u32_j = 200U * usec;		  // 200 is constant for V4, it accuracy must be checked.

	for (u32_i=0; u32_i<=u32_j; u32_i++)
	{

	}; // Only approximate delay
}















