/*
 * api_adc.h
 *
 *  Created on: Dec 18, 2019
 *      Author: volokitin
 */

#ifndef SRC_API_ADC_H_
#define SRC_API_ADC_H_

#include "stm32l4xx_hal.h"

#define CONVERSION_COUNT 3690

void api_adc_get_data(uint32_t u32_index);
void api_adc_send_adc_data(void);
void api_adc_add_data_to_buffer(uint16_t u16_data, uint32_t u32_index);

#endif /* SRC_API_ADC_H_ */
