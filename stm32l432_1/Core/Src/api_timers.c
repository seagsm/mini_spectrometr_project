/*
 * api_timers.c
 *
 *  Created on: Dec 18, 2019
 *      Author: volokitin
 */

#include "api_timers.h"
#include "api_communication.h"
#include "api_sys_state.h"

void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{   static uint32_t u32_counter = 0;
	static uint32_t u32_local_counter = 0;

	switch((uint32_t)htim->Instance)
	{
		case (uint32_t)TIM6:
#if 0
				if(u32_local_counter == 0)
				{
					//HAL_GPIO_TogglePin(GPIOB, GPIO_PIN_5);
					HAL_GPIO_WritePin(GPIOB, GPIO_PIN_5, GPIO_PIN_SET);

					u32_counter++;
				}
				if(u32_local_counter == 1)
				{
					HAL_GPIO_WritePin(GPIOB, GPIO_PIN_5, GPIO_PIN_RESET);
					api_adc_get_data(u32_counter);
				}
#endif
				switch(u32_local_counter)
				{
					case 0:
						HAL_GPIO_WritePin(GPIOB, GPIO_PIN_5, GPIO_PIN_SET);
						break;
					case 1:
						HAL_GPIO_WritePin(GPIOB, GPIO_PIN_5, GPIO_PIN_RESET);
						api_adc_get_data(u32_counter);
						break;
					case 2:
						HAL_GPIO_WritePin(GPIOB, GPIO_PIN_5, GPIO_PIN_SET);
						break;
					case 3:
						HAL_GPIO_WritePin(GPIOB, GPIO_PIN_5, GPIO_PIN_RESET);
						break;
					case 4:
						HAL_GPIO_WritePin(GPIOB, GPIO_PIN_5, GPIO_PIN_SET);
						break;
					case 5:
						HAL_GPIO_WritePin(GPIOB, GPIO_PIN_5, GPIO_PIN_RESET);
						break;
					case 6:
						HAL_GPIO_WritePin(GPIOB, GPIO_PIN_5, GPIO_PIN_SET);
						break;
					case 7:
						HAL_GPIO_WritePin(GPIOB, GPIO_PIN_5, GPIO_PIN_RESET);
						u32_counter++;
						break;
					default:
						break;
				}
				u32_local_counter++;

				if(u32_local_counter >= 8)
				{
					u32_local_counter = 0;
				}

				if(u32_counter >= 3690)
				{
					u32_counter = 0;
					u32_local_counter = 0;
					HAL_TIM_Base_Stop_IT(htim);
					api_sys_set_state(SEND_DATA_TO_UART);
				}
			break;
		default:
			break;
	}
}




