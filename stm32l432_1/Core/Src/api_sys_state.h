/*
 * api_sys_state.h
 *
 *  Created on: Dec 18, 2019
 *      Author: volokitin
 */

#ifndef SRC_API_SYS_STATE_H_
#define SRC_API_SYS_STATE_H_

#include "stm32l4xx_hal.h"

typedef enum
{
	SYSTEM_IDLE,
	START_READ_SEQUENSE,
	WAITING_END_OF_READ_SEQUENCE,
	SEND_DATA_TO_UART
} eSysState;

void main_function(void);
void api_sys_set_state(eSysState   e_value);
void api_sys_get_state(eSysState* pe_value);





#endif /* SRC_API_SYS_STATE_H_ */
