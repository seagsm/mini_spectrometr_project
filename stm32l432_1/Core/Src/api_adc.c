/*
 * api_adc.c
 *
 *  Created on: Dec 18, 2019
 *      Author: volokitin
 */

#include "api_adc.h"
#include "api_communication.h"

uint16_t adcLocalBuffer[CONVERSION_COUNT];

void api_adc_add_data_to_buffer(uint16_t u16_data, uint32_t u32_index)
{
	adcLocalBuffer[u32_index] = u16_data;
}


void api_adc_get_data(uint32_t u32_index)
{
	HAL_ADC_Start(&hadc1);
	adcLocalBuffer[u32_index] = HAL_ADC_GetValue(&hadc1);
}


void api_adc_send_adc_data(void)
{
	api_send_data((uint8_t*)adcLocalBuffer, CONVERSION_COUNT * 2);
}
