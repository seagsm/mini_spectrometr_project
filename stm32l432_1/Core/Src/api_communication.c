/*
 * api_communication.c
 *
 *  Created on: Dec 9, 2019
 *      Author: volokitin
 */


#include "api_communication.h"
#include <string.h>

/* include communication functions: */
#include "api_uart_dma.h"

#include "api_sys_state.h"

uint8_t RxPacket[MAXIMUM_RX_DATA_LENGTH+5];

uint8_t TxCommunicationBuffer[MAXIMUM_DATA_SIZE + 5];


void api_send_data(uint8_t *pu8_data, uint16_t size)
{
	uint16_t u16_crc = 0;
	uint32_t u32_full_packet_size;

	TxCommunicationBuffer[0] = HEADER_VALUE;
	*(uint16_t *)(&TxCommunicationBuffer[1]) = size;

	memcpy(&TxCommunicationBuffer[3], pu8_data, size);
	api_u16_crc(&TxCommunicationBuffer[0], size + 3 , &u16_crc);

	*(uint16_t *)(&TxCommunicationBuffer[3+size]) = u16_crc;

	u32_full_packet_size = size + 5;

	/* here we start uart send function */
	UART_tx_c(&huart1, TxCommunicationBuffer, u32_full_packet_size);

}


/* Should be called from the RxEnd interrupt: */
/* uint8_t *pu8_data return data buffer  */
/* uint16_t * pu16_size Return size of RX data */
void api_get_packet(uint8_t *pu8_data, uint16_t * pu16_size)
{
	ePacketState pars_state = LOOKING_FOR_HEADER;
	uint8_t u8_in_byte = 0;
	uint8_t u8_counter = 0;
	uint16_t u16_data_length = 0;
	uint16_t u16_packet_crc = 0;
	uint16_t u16_local_packet_crc = 0;
	size_t st_tail = 0;
	HAL_StatusTypeDef hal_return = HAL_OK;

	get_dma_USART_tail(COMMUNICATION_LINE, &st_tail);

	while(hal_return == HAL_OK)
	{
		api_uart_read(COMMUNICATION_LINE, &u8_in_byte, &hal_return);
		if(hal_return == HAL_OK)
		{
			switch(pars_state)
			{
				case LOOKING_FOR_HEADER:
					if(u8_in_byte == HEADER_VALUE)
					{
						pars_state = GET_PACKET_LENGTH_LSB;
						pu8_data[0] = u8_in_byte;
					}
					break;

				case GET_PACKET_LENGTH_LSB:
					u16_data_length = u8_in_byte;
					pu8_data[1] = u8_in_byte;
					pars_state = GET_PACKET_LENGTH_MSB;

					break;
				case GET_PACKET_LENGTH_MSB:
					u16_data_length = u16_data_length + (u8_in_byte << 8);
					if(u16_data_length > MAXIMUM_RX_DATA_LENGTH)
					{
						pars_state = WRONG_PACKET;
					}
					else
					{
						pu8_data[2] = u8_in_byte;
						pars_state = GET_DATA;
						u8_counter = 0;
					}
					break;

				case GET_DATA:
					pu8_data[u8_counter + 3] = u8_in_byte;
					u8_counter++;
					if(u8_counter >= (u16_data_length) )
					{
						pars_state = GET_CRC_LSB;
					}
					break;

				case GET_CRC_LSB:
					u16_packet_crc  = u8_in_byte;
					pars_state = GET_CRC_MSB;

					break;
				case GET_CRC_MSB:
					u16_packet_crc  = u16_packet_crc + (u8_in_byte << 8);
					pars_state = GET_CRC_MSB;
					api_u16_crc(pu8_data, u16_data_length + 3, &u16_local_packet_crc);
					if(u16_local_packet_crc == u16_packet_crc)
					{
						*pu16_size = u16_data_length + 3;
						pars_state = CRC_OK;
					}
					else
					{
						pars_state = WRONG_PACKET;
					}
					break;
				case CRC_OK:


					break;

				case WRONG_PACKET:
					/* Return tail counter to original state: */
					set_dma_USART_tail(COMMUNICATION_LINE, st_tail);
					/* Read wrong header, to move tail counter: */
					api_uart_read(COMMUNICATION_LINE, &u8_in_byte, &hal_return);
					/* set to looking state: */
					pars_state = LOOKING_FOR_HEADER;
					break;

				default:
					pars_state = LOOKING_FOR_HEADER;
					break;

			}/* end of switch*/

			if(pars_state == CRC_OK)
			{
				break;
			}

		}/* if(hal_return == HAL_OK)*/
		else
		{
			/* Here can be implemented check for HAL_BUSY. */
		}

	}/* end of while(hal_return == HAL_OK)*/


}


void api_communication_rx_interrupt_handler(void)
{
	uint32_t isrflags   = READ_REG(huart1.Instance->ISR);
	uint16_t u16_size = 0;

	uint32_t u32_counter = 0;
	uint32_t u32_frame_counter = 0;
	/*
	 * 	This bit is set by hardware when an Idle Line is detected. An interrupt is generated if
		IDLEIE=1 in the LPUART_CR1 register. It is cleared by software, writing 1 to the IDLECF in
		the LPUART_ICR register.
	 *
	 *
	 * */
	if ((isrflags & USART_ISR_IDLE) != 0U)
	{
		api_get_packet(RxPacket, &u16_size);

		// Parsing data: TODO: create separate file
		//
		if((RxPacket[0] == 0x73) && (RxPacket[1] == 0x02) && (RxPacket[2] == 0x00) )
		{
			if((RxPacket[3] == 0x80) && (RxPacket[4] == 0x01))
			{
				api_sys_set_state(START_READ_SEQUENSE);
			}
		}
#if 0
			u32_counter = 0;
			u32_frame_counter = 0;
			while(1)
			{
				if(u32_frame_counter < 1)
				{

					if(u32_counter < CONVERSION_COUNT)
					{
						HAL_ADC_Start(&hadc1);
						adcLocalBuffer[u32_frame_counter][u32_counter] = HAL_ADC_GetValue(&hadc1);
						u32_counter++;
					}
					else
					{
						u32_counter = 0;
						u32_frame_counter++;

					}
				}
				else
				{
					api_send_data((uint8_t*)adcLocalBuffer, u32_frame_counter * CONVERSION_COUNT * 2);
					break;
				}
			}
		}
#endif



#if 0

		u32_counter = 0;
		while(1)
		{
			if(u32_counter < CONVERSION_COUNT)
			{
				HAL_ADC_Start(&hadc1);
				adcLocalBuffer[u32_frame_counter][u32_counter] = HAL_ADC_GetValue(&hadc1);
				u32_counter++;
			}
			else
			{
				u32_counter = 0;
				api_send_data((uint8_t*)adcLocalBuffer, CONVERSION_COUNT * 2);
				break;
			}
		}
#endif
		/* ECHO: */
		//api_send_data((uint8_t*)&u16_value, 2);

		SET_BIT(huart1.Instance->ICR, USART_ICR_IDLECF);
	}
}


void api_u16_crc(uint8_t * pu8_data, uint16_t size, uint16_t *pu16_crc)
{
	uint16_t u16_crc = 0;
	uint16_t u16_counter = 0;

	while(u16_counter < size)
	{
		u16_crc = u16_crc + pu8_data[u16_counter];
		u16_counter++;
	}
	*pu16_crc = u16_crc;
}








