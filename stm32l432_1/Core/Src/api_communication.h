/*
 * api_communication.h
 *
 *  Created on: Dec 9, 2019
 *      Author: volokitin
 */

#ifndef SRC_API_COMMUNICATION_H_
#define SRC_API_COMMUNICATION_H_

#include "stm32l4xx_hal.h"
#include "api_adc.h"


#define MAXIMUM_DATA_SIZE 			CONVERSION_COUNT * 2 //4096
#define MAXIMUM_RX_DATA_LENGTH 		(256 - 5)
#define HEADER_VALUE 				0x73
#define COMMUNICATION_LINE 			USART1




extern ADC_HandleTypeDef hadc1;

typedef enum
{
	LOOKING_FOR_HEADER,
	GET_PACKET_LENGTH_LSB,
	GET_PACKET_LENGTH_MSB,
	GET_DATA,
	GET_CRC_LSB,
	GET_CRC_MSB,
	CRC_OK,
	WRONG_PACKET

} ePacketState;









extern uint8_t TxCommunicationBuffer[MAXIMUM_DATA_SIZE + 5];

void api_send_data(uint8_t *pu8_data, uint16_t size);
void api_get_packet(uint8_t *pu8_data, uint16_t * pu16_size);
void api_communication_rx_interrupt_handler(void);




void api_u16_crc(uint8_t * pu8_data, uint16_t size, uint16_t *pu16_crc);




#endif /* SRC_API_COMMUNICATION_H_ */
