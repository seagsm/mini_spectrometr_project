/*
 * api_uart_dma.h
 *
 *  Created on: Nov 5, 2019
 *      Author: volokitin
 */

#ifndef API_UART_DMA_H_
#define API_UART_DMA_H_


#include "stm32l4xx_hal.h"

extern UART_HandleTypeDef huart1;
extern UART_HandleTypeDef huart2;

HAL_StatusTypeDef UART_ID_init(uint8_t u8_interface_id);

void UART_USART1_read_c(uint8_t *pu8_value, HAL_StatusTypeDef *prdr_return);
void UART_USART2_read_c(uint8_t *pu8_value, HAL_StatusTypeDef *prdr_return);
void get_dma_USART_tail(USART_TypeDef *usart, size_t *pst_tail);
void set_dma_USART_tail(USART_TypeDef *usart, size_t st_tail);

void api_uart_read(USART_TypeDef *usart, uint8_t *pu8_value, HAL_StatusTypeDef *prdr_return);
void UART_flush(uint8_t u8_interface_id);
HAL_StatusTypeDef UART_tx_c(UART_HandleTypeDef *UartHandle, uint8_t * pu8_buff, uint32_t u32_len);



void api_usleep(uint32_t usec);

#endif /* API_UART_DMA_H_ */
