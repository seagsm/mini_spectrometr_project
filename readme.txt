https://hackaday.io/project/9829-linear-ccd-module
This translates to the following:

SH must go high with a delay (t2) of between 100 and 1000 ns after ICG goes low.
SH must stay high for (t3) a minium of 1000 ns.
ICG must go high with a delay (t1) of minimum 1000 ns after SH goes low.
This is all taken care of by the timers in the STM32F401RE. 
In fact once they're set up the MCU is not doing any work. 
The only thing the user has to do is to choose ICG-periods that are multiples of the SH-period.
TCD1304
http://megapolis.educom.ru/assets/media/practical-problems-physics-ru-2017.pdf