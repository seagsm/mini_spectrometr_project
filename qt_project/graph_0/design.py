# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'design.ui'
#
# Created by: PyQt5 UI code generator 5.13.2
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(1014, 877)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.verticalLayout = QtWidgets.QVBoxLayout(self.centralwidget)
        self.verticalLayout.setObjectName("verticalLayout")
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.btnReadDevice = QtWidgets.QPushButton(self.centralwidget)
        self.btnReadDevice.setObjectName("btnReadDevice")
        self.horizontalLayout.addWidget(self.btnReadDevice)
        self.btnBrowse = QtWidgets.QPushButton(self.centralwidget)
        self.btnBrowse.setObjectName("btnBrowse")
        self.horizontalLayout.addWidget(self.btnBrowse)
        self.btnSaveData = QtWidgets.QPushButton(self.centralwidget)
        self.btnSaveData.setObjectName("btnSaveData")
        self.horizontalLayout.addWidget(self.btnSaveData)
        self.btnLoadData = QtWidgets.QPushButton(self.centralwidget)
        self.btnLoadData.setObjectName("btnLoadData")
        self.horizontalLayout.addWidget(self.btnLoadData)
        self.ckbAutoSave = QtWidgets.QCheckBox(self.centralwidget)
        self.ckbAutoSave.setChecked(True)
        self.ckbAutoSave.setObjectName("ckbAutoSave")
        self.horizontalLayout.addWidget(self.ckbAutoSave)
        self.chkTimerOn = QtWidgets.QCheckBox(self.centralwidget)
        self.chkTimerOn.setObjectName("chkTimerOn")
        self.horizontalLayout.addWidget(self.chkTimerOn)
        self.verticalLayout.addLayout(self.horizontalLayout)
        self.graphWidget = PlotWidget(self.centralwidget)
        self.graphWidget.setObjectName("graphWidget")
        self.verticalLayout.addWidget(self.graphWidget)
        MainWindow.setCentralWidget(self.centralwidget)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.btnReadDevice.setText(_translate("MainWindow", "Read Data"))
        self.btnBrowse.setText(_translate("MainWindow", "Save Image"))
        self.btnSaveData.setText(_translate("MainWindow", "Save Data"))
        self.btnLoadData.setText(_translate("MainWindow", "Load Data"))
        self.ckbAutoSave.setText(_translate("MainWindow", "AutoSave"))
        self.chkTimerOn.setText(_translate("MainWindow", "TimerOn"))
from pyqtgraph import PlotWidget
