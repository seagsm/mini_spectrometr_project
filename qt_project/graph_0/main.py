import sys  # sys нужен для передачи argv в QApplication
import os  # sys нужен для передачи argv в QApplication
from PyQt5 import QtWidgets
from PyQt5 import QtCore
import glob
import serial
from datetime import datetime
import random
import design
import pyqtgraph.exporters as exporters
import time

# Plot Widget
# https://www.learnpyqt.com/courses/qt-creator/embed-pyqtgraph-custom-widgets-qt-app/
# pyuic5 path/to/design.ui -o output/path/to/design.py
# https://pythonprogramminglanguage.com/pyqtgraph-plot/
# Save to file
# https://programtalk.com/python-examples/pyqtgraph.exporters.ImageExporter/
# https://programtalk.com/vs2/?source=python/3157/binwalk/src/binwalk/core/common.py
# Try it
# https://github.com/ZELLMECHANIK-DRESDEN/pyqtgraph/tree/develop/pyqtgraph

class ExampleApp(QtWidgets.QMainWindow, design.Ui_MainWindow):
    def __init__(self):
        super().__init__()
        self.setupUi(self)  # Design initializing
        # timer = QtCore.QTimer(self)
        # timer.timeout.connect(self.get_data)
        # timer.start(200)
        self.read_data_buffer = [1,1,2,3,4]
        self.btnBrowse.clicked.connect(self.save_image)
        self.btnReadDevice.clicked.connect(self.get_data)
        self.btnSaveData.clicked.connect(self.save_data)
        self.btnLoadData.clicked.connect(self.load_data)
        self.graphWidget.setWindowTitle('pyqtgraph example: Legend')
        #self.input_packet_size = (4096 + 5) # ((4096 * 10) + 5)
        self.input_packet_size = ((3690 * 2) + 5)  # ((4096 * 10) + 5)
        self.FILE_WIDTH = 1024
        #data = [random.random() for i in range(self.input_packet_size)]
        #self.graphWidget.setBackground('#f0f0f0')
        #self.graphWidget.showGrid(x=True, y=True)
        #self.my_plot = self.graphWidget.plot(data, pen='g', symbol='o', symbolPen='r', symbolBrush=0.2,  name='blue')
        self.my_plot = self.plot_init(int((self.input_packet_size-5)/2))


    def plot_init(self, data_size):
        self.graphWidget.setBackground('#f0f0f0')
        # We cann't clean data set from Graph, so, we will change data-set already placed on Graph.
        # But we need do normal plot operation during initialisation.
        data = [random.random() for i in range(data_size)]
        # my_plot = self.graphWidget.plot(data, pxMode=False, pen='g', symbol='o', symbolPen='w', symbolBrush=0.2, size=.5, name='blue')
        # my_plot = self.graphWidget.plot(data, pen='g', symbol='o', symbolPen='r', symbolBrush=0.2, name='blue')
        my_plot = self.graphWidget.plot(data, pen='r', name='blue')
        self.graphWidget.showGrid(x=True, y=True)
        self.graphWidget.addLegend()
        # set properties
        self.graphWidget.setLabel('left', 'Value', units='Unit')
        self.graphWidget.setLabel('bottom', 'Lambda', units='Ang')
        self.graphWidget.setWindowTitle('pyqtgraph plot')
        return my_plot

    def plot_data(self, in_data):
        self.my_plot.setData(in_data)

    def browse_folder(self):
        self.listWidget.clear()  # На случай, если в списке уже есть элементы
        directory = QtWidgets.QFileDialog.getExistingDirectory(self, "Get folder")
        # открыть диалог выбора директории и установить значение переменной
        # равной пути к выбранной директории

        if directory:  # не продолжать выполнение, если пользователь не выбрал директорию
            for file_name in os.listdir(directory):  # для каждого файла в директории
                self.listWidget.addItem(file_name)  # добавить файл в listWidget

    def serial_ports(self):
        if sys.platform.startswith('win'):
            ports = ['COM%s' % (i + 1) for i in range(256)]
        elif sys.platform.startswith('linux') or sys.platform.startswith('cygwin'):
            # this excludes your current terminal "/dev/tty"
            ports = glob.glob('/dev/tty[A-Za-z]*')
        elif sys.platform.startswith('darwin'):
            ports = glob.glob('/dev/tty.*')
        else:
            raise EnvironmentError('Unsupported platform')
        result = []
        for port in ports:
            try:
                s = serial.Serial(port)
                s.close()
                result.append(port)
            except (OSError, serial.SerialException):
                pass
        return result

    def save_data(self,):
        fname = "ECG_%d" % time.time()
        out_data_file = os.path.join(os.getcwd(), os.path.basename(fname + ".dat"))
        i = 0
        byte_data = []
        while i < len(self.read_data_buffer):
            msb = (self.read_data_buffer[i] & 0xFF00) >> 8
            lsb = self.read_data_buffer[i] & 0x00FF
            byte_data.append(lsb)
            byte_data.append(msb)
            i = i + 1
        data_byte_array = bytearray(byte_data)
        data_file = open(out_data_file, "wb")
        data_file.write(data_byte_array)
        data_file.close()

    def load_data(self):
        #plot_data(self, in_data)
        #directory = QtWidgets.QFileDialog.getExistingDirectory(self, "Get folder")
        # открыть диалог выбора директории и установить значение переменной
        # равной пути к выбранной директории
        #if directory:  # не продолжать выполнение, если пользователь не выбрал директорию
        open_file_name = QtWidgets.QFileDialog.getOpenFileName(None, 'Open file')[0]
        if open_file_name:
            binary_file = open(str(open_file_name), "rb")
            binary_data = binary_file.read()
            i = 0
            self.read_data_buffer = []
            while i < len(binary_data):
                self.read_data_buffer.append(binary_data[i + 1] * 256 + binary_data[i])
                i = i + 2
            self.plot_data(self.read_data_buffer)

#            for file_name in os.listdir(directory):  # для каждого файла в директории
#                self.listWidget.addItem(file_name)  # добавить файл в listWidget


    def send_command(self, communication, command ):
        data = bytearray()
        data.append(0x73)
        command_lenght = len(command)
        data.append(command_lenght & 0xFF)
        data.append((command_lenght & 0xFF00)>>8)
        data = data + command
        local_crc = self.packet_crc(data)
        data.append(local_crc & 0xFF)
        data.append((local_crc & 0xFF00)>>8)
        communication.write(data)


    def packet_crc(self, data):
        i = 0
        local_crc = 0
        while i < len(data):
            local_crc = local_crc + data[i]
            i = i + 1
        return local_crc & 0xFFFF

    def get_data(self):
        print(self.serial_ports())
        counter = 0
        s = serial.Serial("COM3")
        s.baudrate = 115200
        data = bytearray(b'\x80\x01')  # 0x80 it is command identification, 0x01 - command number (read frame)
        self.send_command(s, data)
        in_bytes = bytearray()
        received_data = 0
        while s.is_open:
            local_read = s.read()
            my_bytes = bytearray(local_read)
            in_bytes = in_bytes + my_bytes
            data_size = len(my_bytes)

            if data_size > 0:
                counter = counter + 1
                received_data = float(received_data + data_size)
                print(received_data)
                if received_data == self.input_packet_size:
                    i = 0
                    self.read_data_buffer = []
                    while i < (self.input_packet_size - 5):
                        self.read_data_buffer.append(in_bytes[i+1 + 3]*256 + in_bytes[i+3])
                        i = i + 2
                    self.plot_data(self.read_data_buffer)
                    if self.ckbAutoSave.isChecked():
                        self.save_image()
                        self.save_data()
                    break
        s.close()

    def save_image(self):
        fname = "ECG_%d" % time.time()
        out_file = os.path.join(os.getcwd(), os.path.basename(fname + ".jpg"))
        # exporters.ImageExporter is different in different versions of pyqtgraph
        try:
            exporter = exporters.ImageExporter(self.graphWidget.plotItem)
        except TypeError:
            exporter = exporters.ImageExporter.ImageExporter(self.graphWidget.plotItem)
        exporter.parameters()['width'] = self.FILE_WIDTH
        exporter.export(out_file)

def main():
    app = QtWidgets.QApplication(sys.argv)  # Новый экземпляр QApplication
    window = ExampleApp()  # Создаём объект класса ExampleApp
    window.show()  # Показываем окно
    app.exec_()  # и запускаем приложение

if __name__ == '__main__':  # Если мы запускаем файл напрямую, а не импортируем
    main()  # то запускаем функцию main()